drop database if exists ordermanagement;
create database if not exists ordermanagement;

use ordermanagement;

drop table if exists clients;
create table if not exists clients
(
  ID int unique primary key,
	nume varchar(50),
  prenume varchar(50),
  adresa varchar(100)
);

drop table if exists products;
create table if not exists products
(
  ID int unique primary key,
	nume varchar(50),
	cantitate int,
	pret int
);

drop table if exists orders;
create table if not exists orders
(
  ID int primary key,
	ID_client int,
	ID_produs int,
	cantitate int
);

