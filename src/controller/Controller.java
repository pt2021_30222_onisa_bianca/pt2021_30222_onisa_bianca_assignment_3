package controller;

import dataAccessLayer.ConnectionFactory;
import model.Client;
import model.Order;
import model.Product;
import businessLayer.OperatiiClient;
import businessLayer.OperatiiProduct;
import presentation.ExceptionFr;
import presentation.Frame;
import presentation.Receipt;
import presentation.Stock;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    public Controller()
    {
        Frame fr=new Frame();
        fr.setVisible(true);

        fr.getBadd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID, nume, prenume, adresa;
                    ID = fr.getTid().getText();
                    nume = fr.getTnume().getText();
                    prenume = fr.getTprenume().getText();
                    adresa = fr.getTadresa().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    Client cl = new Client(Integer.parseInt(ID), nume, prenume, adresa);
                    businessLayer.OperatiiClient.insertClient(cl);
                    con.close();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });

        fr.getBdel().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID;
                    ID = fr.getTid().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    businessLayer.OperatiiClient.deleteClient(Integer.parseInt(ID));
                    con.close();
                } catch (Exception ex) {
                    ExceptionFr exceptionFr = new ExceptionFr();
                }
            }
        });

        fr.getBupd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID, nume, prenume, adresa;
                    ID = fr.getTid().getText();
                    nume = fr.getTnume().getText();
                    prenume = fr.getTprenume().getText();
                    adresa = fr.getTadresa().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    Client cl = new Client(Integer.parseInt(ID), nume, prenume, adresa);
                    businessLayer.OperatiiClient.updateClient(cl);
                    con.close();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });

        fr.getRefresh().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    fr.setVisible(false);
                    fr.getContentPane().remove(fr.getTableCl());
                    JTable tableCl = OperatiiClient.showTableClients();
                    tableCl.setBounds(485, 450, 600, 200);
                    fr.setTableCl(tableCl);
                    fr.setVisible(true);
                    fr.getContentPane().add(tableCl);
                    fr.invalidate();
                    fr.validate();
                    fr.repaint();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });

        fr.getBadd2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID1, nume1, cantit1, pret1;
                    ID1 = fr.getTidprod().getText();
                    nume1 = fr.getTnumeprod().getText();
                    cantit1 = fr.getTcantit().getText();
                    pret1 = fr.getTpret().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    Product prod = new Product(Integer.parseInt(ID1), nume1, Integer.parseInt(cantit1), Integer.parseInt(pret1));
                    businessLayer.OperatiiProduct.insertProduct(prod);
                    con.close();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });

        fr.getBdel2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID;
                    ID = fr.getTidprod().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    businessLayer.OperatiiProduct.deleteProduct(Integer.parseInt(ID));
                    con.close();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });

        fr.getBupd2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String ID1, nume1, cantit1, pret1;
                    ID1 = fr.getTidprod().getText();
                    nume1 = fr.getTnumeprod().getText();
                    cantit1 = fr.getTcantit().getText();
                    pret1 = fr.getTpret().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();
                    Product prod = new Product(Integer.parseInt(ID1), nume1, Integer.parseInt(cantit1), Integer.parseInt(pret1));
                    businessLayer.OperatiiProduct.updateProduct(prod);
                    con.close();
                } catch (Exception ex) {
                    ExceptionFr exceptionFr = new ExceptionFr();
                }
            }
        });

        fr.getRefresh2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                ConnectionFactory con=new ConnectionFactory();
                con.createConnection();
                con.getConnection();
                fr.setVisible(false);
                JTable tableProd= OperatiiProduct.showTableProducts();
                tableProd.setBounds(485,450,600,200);
                fr.setTableCl(tableProd);
                fr.setVisible(true);
            }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }}
        });

        fr.getBadd3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String idO, idC, idP, cant;
                    idO = fr.getTidord().getText();
                    idC = fr.getTidclo().getText();
                    idP = fr.getTidprodo().getText();
                    cant = fr.getTcantito().getText();

                    ConnectionFactory con = new ConnectionFactory();
                    con.createConnection();
                    con.getConnection();

                    Client cl = businessLayer.OperatiiClient.findById(Integer.parseInt(idC));
                    Product prod = businessLayer.OperatiiProduct.findById(Integer.parseInt(idP));
                    if (cl != null && prod != null && prod.getCantitate() >= Integer.parseInt(cant)) {
                        Order ord = new Order(Integer.parseInt(idO), Integer.parseInt(idC), Integer.parseInt(idP), Integer.parseInt(cant));
                        prod.setCantitate(prod.getCantitate() - Integer.parseInt(cant));
                        businessLayer.OperatiiProduct.updateProduct(prod);
                        businessLayer.OperatiiOrder.insertOrder(ord);
                        int x=Integer.parseInt(cant)*prod.getPret();
                        Receipt r=new Receipt(x);

                    }
                    else if (cl != null && prod != null && prod.getCantitate() < Integer.parseInt(cant))
                    {
                        Stock stock=new Stock();
                    }
                    con.close();
                }
                catch (Exception ex)
                {
                    ExceptionFr exceptionFr= new ExceptionFr();
                }
            }
        });
    }
}
