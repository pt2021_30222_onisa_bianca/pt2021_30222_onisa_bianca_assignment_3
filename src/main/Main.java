package main;

import dataAccessLayer.ConnectionFactory;
import controller.Controller;
import model.Client;
import model.Order;
import model.Product;
import presentation.ExceptionFr;

public class Main {
    public static void main(String []args)
    {
        ConnectionFactory con=new ConnectionFactory();
        con.createConnection();
        Controller c=new Controller();

        Client cl=new Client(5,"Popaaaaa","Tudor","Pandurilor 54");
       // operatii.OperatiiClient.insertClient(cl);
        //operatii.OperatiiClient.deleteClient(3);
        //operatii.OperatiiClient.updateClient(cl);

        Order ord=new Order(3,3,4,92);
       // operatii.OperatiiOrder.insertOrder(ord);
       //  operatii.OperatiiOrder.deleteOrder(10);
        //operatii.OperatiiOrder.updateOrder(ord);

        Product prod=new Product(9,"unt",190,3);
        //operatii.OperatiiProduct.insertProduct(prod);
        //operatii.OperatiiProduct.deleteProduct(2);
        //operatii.OperatiiProduct.updateProduct(prod);

    }
}
