package businessLayer;

import dataAccessLayer.ConnectionFactory;
import model.Client;
import presentation.ExceptionFr;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.ArrayList;

public class OperatiiClient {

    private static ArrayList<Client> listCl=new ArrayList<Client>();

    public static Client findById(int id)
    {
        Client cl=null;
        Connection con= ConnectionFactory.getConnection();
        PreparedStatement find=null;
        ResultSet rs=null;
        try{
            String nume,prenume,adresa;
            find=con.prepareStatement("select * from clients where id=?");
            find.setInt(1,id);
            rs=find.executeQuery();
            rs.next();
            nume=rs.getString("Nume");
            prenume=rs.getString("Prenume");
            adresa=rs.getString("Adresa");
            cl=new Client(id,nume,prenume,adresa);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            ExceptionFr exceptionFr=new ExceptionFr();
        }
        return cl;
    }

    public static void insertClient(Client client)
    {
        Connection con= ConnectionFactory.getConnection();
        PreparedStatement insert=null;
        try
        {
            insert=con.prepareStatement("insert into clients (id,nume,prenume,adresa) values (?,?,?,?)");
            insert.setInt(1,client.getID());
            insert.setString(2, client.getNume());
            insert.setString(3, client.getPrenume());
            insert.setString(4, client.getAdresa());
            insert.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            ExceptionFr exceptionFr=new ExceptionFr();
        }
    }

    public static void deleteClient(int id)
    {
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement delete=null;
        try
        {
            delete=con.prepareStatement("delete from clients where id=?");
            delete.setInt(1,id);
            delete.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            ExceptionFr exceptionFr=new ExceptionFr();
        }
    }

    public static void updateClient(Client client)
    {
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement update=null;
        try
        {
            update=con.prepareStatement("update clients set nume=?, prenume=?, adresa=? where id=?");
            update.setString(1,client.getNume());
            update.setString(2,client.getPrenume());
            update.setString(3,client.getAdresa());
            update.setInt(4,client.getID());
            update.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            ExceptionFr exceptionFr=new ExceptionFr();
        }
    }

    public static JTable showTableClients()
    {
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement show=null;
        DefaultTableModel tabel=new DefaultTableModel();
        ResultSet rs=null;
        tabel.addColumn("ID");
        tabel.addColumn("Nume");
        tabel.addColumn("Prenume");
        tabel.addColumn("Adresa");
        JTable tableCl=new JTable(tabel);

        try
        {
            show=con.prepareStatement("select * from clients");
            rs=show.executeQuery();

            while(rs.next())
            {
                Client client=new Client(rs.getInt("ID"),rs.getString("Nume"),rs.getString("Prenume"),rs.getString("Adresa"));
                listCl.add(client);
                Object obj=new Object[]{rs.getInt("ID"),rs.getString("Nume"),rs.getString("Prenume"),rs.getString("Adresa")};
                tabel.addRow((Object[]) obj);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            ExceptionFr exceptionFr=new ExceptionFr();
        }
        return tableCl;
    }

    public static ArrayList<Client> getListCl() {
        return listCl;
    }

    public void setListCl(ArrayList<Client> listCl) {
        this.listCl = listCl;
    }
}
