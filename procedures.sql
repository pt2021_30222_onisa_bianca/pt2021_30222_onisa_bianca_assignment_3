drop procedure if exists addClient;
create procedure addClient(id1 int,nume1 varchar(50) ,prenume1 varchar(50),adresa1 varchar(50))
begin 
insert into clients values (id1,nume1,prenume1,adresa1);
end;

drop procedure if exists editClient;
create procedure editClient(id1 int,nume1 varchar(50) ,prenume1 varchar(50),adresa1 varchar(50))
begin
		 update clients
		 set nume:=nume1,prenume:=prenume1,adresa:=adresa1 where id=id1;
end;

drop procedure if exists deleteClient;
create procedure deleteClient(id1 int)
begin
			delete from clients where clients.id=id1;
end;

drop procedure if exists addProduct;
create procedure addProduct(id1 int,nume1 varchar(50) ,cantitate1 int, pret1 int)
begin 
insert into products values (id1,nume1,cantitate1,pret1);
end;

drop procedure if exists editProduct;
create procedure editProduct(id1 int,nume1 varchar(50) ,cantitate1 int, pret1 int)
begin
		 update products
		 set nume:=nume1,cantitate:=cantitate1,pret:=pret1 where id=id1;
end;

drop procedure if exists deleteProduct;
create procedure deleteProduct(id1 int)
begin
			delete from products where products.id=id1;
end;




