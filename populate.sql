insert into clients values
(1,"Popa","Bogdan","Observator 10"),
(2,"Toma","Andrei","Bucuresti 23"),
(3,"Raica","Elena", "Closca 12"),
(4,"Popan","Irina", "Tomsa 20"),
(5,"Calin", "Alex", "Pandurilor 74");

insert into products values
(1, "paine", 200,3),
(2, "cereale", 15, 7),
(3, "lapte", 45, 5),
(4, "apa", 120, 4),
(5, "paste", 58, 6);

insert into orders values
(1,2,3,10),
(2,1,3,13),
(3,5,2,25),
(4,3,2,98),
(5,2,4,37);